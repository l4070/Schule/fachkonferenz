# Kollaborative Dokumentenerstellung mit LaTeX

Workshop im Rahmen der INFOS2021 https://dl.gi.de/handle/20.500.12116/36970

Marei Peischl <marei@peitex.de> & André Hilbig <hilbig@uni-wuppertal.de>

## Fachkonferenz

Demo Projekt für eine Einladung zu einer Fachkonferenz und das daraus resultierende Protokoll mit angepasster Syntax.

### Lokal bauen

Inklusive Submodul klonen:

```
git clone --recurse-submodules git@gitlab.com:l4070/Schule/fachkonferenz.git
```

Anschließend im Projektordner

```
latexmk -r latex-markdown-templates/CI.rc <Markdown-Datei.md>
```

ausführen, z.B.

```
latexmk -r latex-markdown-templates/CI.rc fachkonferenz-protokoll-aus-einladung.md
```


### Struktur des Verzeichnisses

#### Inhaltsdateien
- fachkonferenz-einladung.md Konfiguration und Text des Einladungsbriefes
- fachkonferenz-protokoll-aus-einladung.md Kopie der fachkonferenz-einladung.md mit Anpassungen für das Protokoll


#### Templatedateien 
- template-fachkonferenz-einladung.tex
- template-fachkonferenz-aus-einladung.tex
- maxmuster.lco: Absenderadressdaten
- schulprotokoll.cls: Klassendatei für Protokolle
