---
template: fachkonferenz-einladung
lco: maxmuster
date: 2020-09-15
time: 14:10
room: 025
---
0. Feststellung der ordnungsgemäßen Einladung und der Beschlussfähigkeit
1. Wahl des FK-Vorsitz
2. Lehrpläne/Perspektiven
3. Digitale Lernplatform 
4. GIT als Versionsverwaltung
5. GIT mit \LaTeX
6. Sonstiges
