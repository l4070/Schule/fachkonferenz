---
template: fachkonferenz-protokoll-aus-einladung
title: 1. Fachkonferenz Informatik
date: 2020-09-15
begintime: 14:10
endtime: 15:00
room: 025
anwesend: Max, Sabine
abwesend: Marlen
---

# 14:10=Feststellung der ordnungsgemäßen Einladung und der Beschlussfähigkeit

- Die Einladung ist ordnungsgemäß erfolgt.
- Die Beschlussfähigkeit ist gegeben.

# 14:16=Wahlen der Fachvorsitzenden

\abstimmung[Vorschlag: Sabine Schuster]{Fachvorsitz}{2}{0}{0}{Frau Schuster gewählt}

# 14:20=Lehrpläne/Perspektiven

- Lehrplan ist nun vollständig für EF und Q.

# 14:40=Digitale Lernplattform

- Dies und das

# 14:50=Sonstiges

- Perspektivisch für das nächste Schuljahr soll eine Exkursion in das Heinz-Nixdorf-Museum geplant
  werden.
