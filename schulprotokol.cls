% -*- coding: utf-8; -*-
% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -+
%                                                                                                  |
% (C)opyright A. Hilbig, mail@andrehilbig.de                                                       |
%             http://www.andrehilbig.de/Publications                                               |
%                                                                                                  | 
% diese Datei: schulprotokol.cls	                                                                 |
%                                                                                                  |
% Das Dokument steht unter der Lizenz: Creative Commons by-nc-sa Version 4.0                       |
% http://creativecommons.org/licenses/by-nc-sa/4.0/deed.de                                         |
%                                                                                                  |
% Nach dieser Lizenz darf das Dokument beliebig kopiert und bearbeitet werden,                     |
% sofern das Folgeprodukt wiederum unter gleichen Lizenzbedingungen vertrieben                     |
% und auf die ursprünglichen Urheber verwiesen wird.                                               |
% Eine kommerzielle Nutzung ist ausdrücklich ausgeschlossen.                                       |
%                                                                                                  |
%                                                                                                  |
% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -+
%                                                                                                  |
\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{schulprotokol}

\LoadClass[a4paper]{scrartcl}

%\newif\ifdraftversion

%\DeclareOption{draft}{\draftversiontrue}

%\ProcessOptions

%\ifbw
%	\colorsecfalse
%\fi
%\RequirePackage[ngerman]{babel}
%\RequirePackage[T1]{fontenc}

\RequirePackage[]{geometry}

\RequirePackage{epic,eepic}
\RequirePackage{graphicx}
\RequirePackage{textpos}
\RequirePackage{xspace}
\RequirePackage{hyperref}
\hypersetup{
	colorlinks=true,
	linkcolor=black,
}
%\RequirePackage[nolinks]{qrcode}
\RequirePackage[german=guillemets,babel]{csquotes}
\RequirePackage{eurosym}

\RequirePackage[dvipsnames]{xcolor}
\RequirePackage{fancybox}

\RequirePackage{multicol}

\RequirePackage{tcolorbox}
\tcbuselibrary{skins}

\RequirePackage{ifthen}
%\RequirePackage{dashrule}
%\RequirePackage{bbding}

\providecommand{\anwesend}{}
\providecommand{\Teilnehmer}[2][]{%
	\hypersetup{pdfauthor=#1}
	\renewcommand{\anwesend}{#2}
}
\providecommand{\abwesend}{}
\providecommand{\Abwesend}[1]{%
	\renewcommand{\abwesend}{#1}
}

\providecommand{\titel}{}
\providecommand{\Titel}[1]{%
	\renewcommand{\titel}{#1}
}
\providecommand{\datum}{}
\providecommand{\Datum}[1]{%
	\renewcommand{\datum}{#1}
}

\providecommand{\raum}{}
\providecommand{\Raum}[1]{%
	\renewcommand{\raum}{#1}
}
\providecommand{\start}{}
\providecommand{\Start}[1]{%
	\renewcommand{\start}{#1\xspace}
}

\providecommand{\schluss}{}
\providecommand{\Schluss}[1]{%
	\renewcommand{\schluss}{#1\xspace}
}

\providecommand{\logo}{}
\providecommand{\Logo}[2][]{\renewcommand{\logo}{\includegraphics[#1,width=0.95\textwidth]{#2}}}


\newcounter{topic}
\setcounter{topic}{0}
\providecommand{\topic}[3][]{%
	\minisec{Topic \thetopic: #3}
	\marginpar{#2\,Uhr}
	\refstepcounter{topic}
}

\providecommand{\entry}[3][]{%
	\marginpar{#2}
	#3
}
\providecommand{\aussage}[2][]{
	#1 \enquote{#2} \color{black}
}

\providecommand{\abstimmung}[6][none]{%
	\vspace{1mm}
	%\marginpar{#1}
	\begin{tcolorbox}[%
		title={\textbf{Abstimmung:} #2},
		skin=bicolor,colbacklower=white!85!black,
	]
%		\hspace{1.5em}
		\begin{minipage}{0.85\textwidth}
			\ifthenelse{\equal{#1}{none}}{}{#1\vspace{1.25em}}

			Zustimmung: #3

		 	Ablehnung: #4

			Enthaltung: #5
		\end{minipage}

		\tcblower

		\textbf{Ergebnis:} #6
	\end{tcolorbox}
%	\vspace{1em}
}

\providecommand{\luecke}[1]{\rule[0pt]{#1}{.5pt}}

\AtBeginDocument{
	\geometry{
		top=1.8cm,
		left=2.5cm,
		right=3cm,
		bottom=2cm,
		includehead,includefoot,
	}
	\begin{minipage}{0.6\textwidth}
		\section*{\titel}
		\begin{tabular}{lp{0.6\textwidth}}
			\textbf{Datum:} & \datum \\
			\textbf{Uhrzeit:} & \start bis \schluss\\
			\textbf{Raum:} & \raum\\
			\textbf{Teilnehmer:} & \anwesend \\
			\textbf{Abwesend:} & \abwesend \\
		\end{tabular}
	\end{minipage}
	\hfil
	\begin{minipage}{0.35\textwidth}
		\logo
	\end{minipage}
	\vspace{1.5em}
	\hrule
	\vspace{1em}
}
